module CarsHelper
  def car_form_path(car)
    car.present? ? admin_car_path(@car) : admin_cars_path
  end

  def car_form_method(car)
    car.present? ? :put : :post
  end

  def car_form_button(car)
    car.present? ? 'Edit' : 'New'
  end

  def total_users(group)
    # post.groups.map { |item| item.users.count }.sum
  end

  def views_percents(car)
    1
    # return '-' if post.views_info.empty? || total_users(post).zero?
    # "#{100 * post.views_info.length / total_users(post)} %"
  end
end
