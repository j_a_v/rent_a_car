module Admin
  class CarsController < Admin::ApplicationController
    before_action :set_car, only: %i(show edit update destroy)

    def index
      @cars = Car.all
                 # .eager_load(:groups)
    end

    def show; end

    def new
      @car_form = ::CarForm.new
    end

    def edit
      @car_form = ::CarForm.new(id: @car.id)
    end

    def create
      @car_form = ::CarForm.new(car_form_params.merge(current_user: current_user))
      if car = @car_form.save
        # image attach from ActiveStorage doesn't work in form object yet
        car.image.attach(params[:car_form][:image]) if params[:car_form][:image].present?
        redirect_to admin_cars_path, notice: 'Car was successfully created.'
      else
        render :new
      end
    end

    def update
      @car_form = ::CarForm.new(car_form_params.merge(current_user: current_user, id: @car.id))
      if @car_form.update
        redirect_to admin_cars_path, notice: 'Car was successfully updated.'
      else
        render :edit
      end
    end

    def destroy
      @car.destroy
      respond_to do |format|
        format.html { redirect_to admin_cars_url, notice: 'Car was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    def set_car
      @car = Car.find(params[:id])
    end

    def car_params
      params.require(:car).permit(:title, :description)
    end

    def car_form_params
      params.require(:car_form).permit(:title, :description, :image)
    end
  end
end
