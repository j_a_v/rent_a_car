class CarsController < ApplicationController
  def index
    @cars = Car.all
              # .joins(:groups).eager_load(:image_attachment)
               # .where('groups.id IN (?)', current_user.groups.pluck(:id))
    authorize @cars
  end

  def show
    @car = Car.find_by(id: params[:id])
    CarViewsService.new(@car, current_user).perform
    authorize @car
  end
end
