class CarForm
  include ActiveModel::Model

  attr_accessor :id, :title, :description, :group_ids, :current_user, :image

  validates :title, presence: true

  def initialize(params = {})
    if params[:id].present?
      @car = Car.find_by(id: params[:id])
      self.title = params[:title].nil? ? @car.title : params[:title]
      self.description = params[:description].nil? ? @car.description : params[:description]
      # self.group_ids = params[:group_ids].nil? ? @car.groups.pluck(:id) : params[:group_ids]
    else
      super
    end
  end

  def save
    return false if invalid?
    begin
      ActiveRecord::Base.transaction do
        @car = Car.create(car_params)
        # @car.groups << Group.where(id: group_ids)
        @car.save!
      end
      # @car.image.attach(:image) # ActionDispatch::Http::UploadedFile object issue
      #
      # @car.image.attach(io: File.open(Rails.root.join("public", "song.jpeg")),
      # filename: 'song.jpeg' , content_type: "image/jpeg")
      NewCarNotificationJob.perform_later(@car)

      @car
    rescue StandardError => error
      errors.add(:base, error.message)

      false
    end
  end

  def update
    return false if invalid?
    begin
      ActiveRecord::Base.transaction do
        @car.update(car_params.slice(:title, :description))
        @car.touch
        # @car.groups = Group.where(id: group_ids)
        @car.save!
      end

      true
    rescue e
      errors.add(:base, e.message)

      false
    end
  end

  private

  def car_params
    {
      title: title,
      description: description,
      user: current_user
    }
  end
end
