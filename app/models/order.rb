class Order < ApplicationRecord
  belongs_to :user
  has_one :car

  validates :car_id, presence: true
  validates :user_id, presence: true
  validates :start_at, presence: true
  validates :end_at, presence: true
end
