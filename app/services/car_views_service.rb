class CarViewsService
  include CarsHelper

  def initialize(car, user)
    @car = car
    @user = user
  end

  def perform

    update_car unless @car.views_info[@user.id.to_s].present?
  end

  private

  def update_car
    @car.views_info[@user.id] = Time.zone.now
    @car.save!
    ActionCable.server.broadcast('stats_revision', car_id: @car.id, views_stats: views_percents(@car))
  end
end
