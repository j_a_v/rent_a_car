App.stats_revision = App.cable.subscriptions.create('StatsRevisionChannel', {
    connected: function() {
    },
    disconnected: function() {
    },
    received: function(data) {
        if (data.car_id != undefined ) {
            $('.' + data.car_id).text(data.views_stats)
        }
    }
});
