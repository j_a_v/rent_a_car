class NewCarNotificationMailer < ApplicationMailer
  def notification(car, user)
    @car = car
    @user = user
    mail(to: @user.email, subject: 'New car notification')
  end
end
