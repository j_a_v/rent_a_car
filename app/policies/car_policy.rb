class CarPolicy < ApplicationPolicy
  def index?
    user.present?
  end

  def show?
    # permission = post.groups.pluck(:id) & user.groups.pluck(:id)
    # return true if permission.present?
    true
  end

  private

  def car
    record
  end
end
