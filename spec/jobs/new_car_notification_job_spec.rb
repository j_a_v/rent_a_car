require 'rails_helper'

describe NewCarNotificationJob do
  it { is_expected.to be_processed_in(:default) }
end
