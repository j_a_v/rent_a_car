require 'rails_helper'

describe CarViewsService do
  let(:car) { FactoryBot.create(:car) }
  let(:user) { FactoryBot.create(:user) }
  let(:views_info) {  { user.id.to_s => 1.day.ago.to_s } }
  let(:viewed_car) { FactoryBot.create(:car, views_info: views_info) }

  describe 'initialize' do
    subject { CarViewsService.new(car, user) }

    it 'sets required attributes' do
      expect(subject.instance_variable_get(:@car)).to eq(car)
      expect(subject.instance_variable_get(:@user)).to eq(user)
    end
  end

  context 'first visit' do
    let(:group) { create(:group) }

    before do
      user.groups << group
      car.groups << group
    end

    subject { CarViewsService.new(car, user).perform }

    it 'updates users views' do
      expect { subject }.to change { car.views_info.length }.from(0).to(1)
    end

    it 'send data to stats revision channel' do
      expect { subject }.to have_broadcasted_to('stats_revision').with(car_id: car.id, views_stats: '100 %')
    end
  end

  context 'second visit' do
    subject { CarViewsService.new(viewed_car, user).perform }

    it 'views info not changed' do
      expect(viewed_car.views_info.length).to eq 1
      expect(viewed_car.views_info[user.id.to_s].to_i).to eq views_info[user.id.to_s].to_i
      subject
      expect(viewed_car.views_info.length).to eq 1
      expect(viewed_car.views_info[user.id.to_s].to_i).to eq views_info[user.id.to_s].to_i
    end
  end
end
