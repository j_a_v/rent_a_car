require 'rails_helper'

RSpec.describe Admin::CarsController, type: :controller do
  let(:admin) { FactoryBot.create(:user, :admin) }
  let(:cars_record) { FactoryBot.create(:cars) }

  before { sign_in admin }

  describe 'GET #index' do
    context 'logged in' do
      it 'returns an array of cars' do
        get :index
        expect(assigns(:cars)).to include(cars_record)
      end
    end
  end

  describe 'GET #new' do
    context 'logged in' do
      it 'build new cars' do
        get :new
        expect(assigns(:car_form)).to be_a(CarForm)
      end
    end
  end

  describe 'GET #edit' do
    context 'logged in' do
      it 'edit car' do
        get :edit, params: { id: car_record.id }
        expect(assigns(:car_form)).to be_a(CarForm)
      end
    end
  end

  describe 'DELETE #destroy' do
    subject { delete :destroy, params: { id: car_record.id } }

    it 'should delete' do
      expect(subject).to redirect_to(admin_cars_path)
      expect(Car.count).to eq 0
    end
  end

  describe 'POST #create' do
    before { allow(controller).to receive_messages(current_user: admin) }
    context 'success' do
      it 'should create' do
        expect(Car.count).to eq 0
        post :create, params: { car_form: attributes_for(:car) }
        expect(response).to redirect_to(admin_cars_path)
        expect(Car.count).to eq 1
        expect(flash[:notice]).to eq('Post was successfully created.')
      end
    end
  end

  describe 'PATCH #update' do
    before { allow(controller).to receive_messages(current_user: admin) }
    context 'success' do
      it 'update' do
        car_record
        expect(Car.count).to eq 1
        patch :update, params: { id: car_record.id, car_form: { title: 'new title' } }
        expect(response).to redirect_to(admin_cars_path)
        expect(Post.count).to eq 1
        car_record.reload
        expect(car_record.title).to eq 'new title'
        expect(flash[:notice]).to eq('Post was successfully updated.')
      end
    end
  end
end
