require 'rails_helper'

RSpec.describe CarsController, type: :controller do
  let(:car) { FactoryBot.create(:car) }
  let(:user) { FactoryBot.create(:user) }
  let(:user_with_cars) { FactoryBot.create(:user, :user_with_cars) }

  describe 'GET #index' do
    context 'not logged in' do
      it 'redirects' do
        get :index
        expect(response).to have_http_status(302)
      end
    end

    context 'logged in' do
      before { sign_in user }

      it 'returns http success' do
        get :index
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'GET #show' do
    context 'not permitted' do
      before { sign_in user }
      it 'redirects' do
        get :show, params: { id: car.id }
        expect(response).to have_http_status(302)
      end
    end

    context 'permitted' do
      before { sign_in user_with_cars }
      it 'returns http success' do
        get :show, params: { id: Car.first.id }
        expect(response).to have_http_status(200)
      end
    end
  end
end
