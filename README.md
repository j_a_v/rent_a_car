Rent a Car
====

System to create cars and distribute them


### Basic system specs:
- Ruby 2.7.4
- Rails 6.0.2

### DB adapter:
- PostgreSQL

### Additional requirement
- Redis

### Configuration
* bundle
* define environment variable ``ENV['SECRET_KEY_BASE']``
* copy ``config/database.example.yml`` into ``config/database.yml``
* set your username and password into ``config/database.yml`` for postgreSQL
* ``rake db:create``
* ``rake db:migrate``
* run rake task to fill DB with users``rake db:fill_users``
* ``rails s``
* ``sidekiq``

### Credentials
- Admin user  
login: ``admin@test.com``
password: ``12345678``

- User  
login: ``user@test.com``
password: ``12345678``
