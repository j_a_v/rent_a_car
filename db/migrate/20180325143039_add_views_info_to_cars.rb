class AddViewsInfoToCars < ActiveRecord::Migration[5.2]
  def change
    add_column :cars, :views_info, :json, default: {}, null: false
  end
end
